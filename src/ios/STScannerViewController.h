//
//  STViewController.h
//  Vericode
//
//  Created by stefano luzardi on 22/08/18.
//

#ifndef STScannerViewController_h
#define STScannerViewController_h

#import <UIKit/UIKit.h>
#import "STCameraManager.h"

@class STScanner;

@interface STScannerViewController : UIViewController <STCameraManagerDelegate>
    @property (weak, nonatomic) IBOutlet STPreviewView *preview;
    @property (weak, nonatomic) IBOutlet UILabel *scanStateLabel;
    @property (weak, nonatomic) STScanner *plugin;

    - (id)initWithPlugin:(STScanner*)plugin callback:(NSString*)callback nibName:(NSString*)nibname;
    - (IBAction)cancelButtonTapped:(id)sender;

@end
#endif /* STScannerViewController_h */
