
/*************************************************************************
 *
 * SCANTRUST CONFIDENTIAL
 * __
 *
 *  [2015] - [2018] ScanTrust SA, PSE - D, EPFL Innovation Park, 1015 Lausanne, Switzerland
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of ScanTrust SA and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to ScanTrust SA
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from ScanTrust SA .
 */

#import <Foundation/Foundation.h>

#ifdef __cplusplus
#include <UrlMatcher.hpp>
#endif

@interface STMatcherResult : NSObject
@property (nonatomic, getter=isMatching) BOOL matches;
@property (nonatomic, strong) NSString *extendedId;
@property (nonatomic, strong) NSString *customKey;

@end

@interface STUrlMatcher : NSObject
+ (STMatcherResult *)belongsToScanTrust:(NSString *)code;
@end
