
/*************************************************************************
 *
 * SCANTRUST CONFIDENTIAL
 * __
 *
 *  [2015] - [2018] ScanTrust SA, PSE - D, EPFL Innovation Park, 1015 Lausanne, Switzerland
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of ScanTrust SA and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to ScanTrust SA
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from ScanTrust SA .
 */

#import <Foundation/Foundation.h>
#import "STCameraManager.h"
#import "STScanResult.h"


@class STLimitedCameraManager;


/**
 Protocol methods to receive scan result from a ScanTrust frame processing from limited camera manager
 */
@protocol STLimitedCameraManagerDelegate <NSObject>
@optional


/**
 Informs the delegate that the camera frame is processed by ScanTrust frame processing with a scan result

 @param cameraManager The limited camera manager object that captured the frame for processing
 @param scanResult The STScanResult instance representing a scan result
 :nodoc:
 */
- (void)cameraManager:(STLimitedCameraManager *)cameraManager didProcessFrameWithScanResult:(STScanResult *)scanResult;


/**
 Informs the delegate that the camera frame is completed processing by ScanTrust frame processing with a scan result

 @param cameraManager The limited camera manager object that captured the frame for processing
 @param scanResult The STScanResult instance representing a scan result
 */
- (void)cameraManager:(STLimitedCameraManager *)cameraManager didCompleteScanWithScanResult:(STScanResult *)scanResult;
@end



/**
 The STLimitedCameraManager does the same camera and preview view setup as the STCameraManager but does not perform
 the image and frame processing required for the ScanTrust authentication. It is limited to checking the ownership
 of the code based on its content.
 */
@interface STLimitedCameraManager : NSObject


/**
 The object that acts as delegate for limited camera manager
 */
@property (nonatomic, strong) id<STLimitedCameraManagerDelegate> delegate;


/**
 Returns the shared limited camera manager instance

 @return The shared limited camera manager instance
 */
+ (STLimitedCameraManager *)sharedInstance;


/**
 To get the camera preview object

 @return The STPreviewView object associated to camera manager instance
 :nodoc:
 */
- (STPreviewView *)getCameraPreviewView;


/**
 Starts the underlying camera object, its preview and starts the ScanTrust frame processing.
 */
- (void)startScanning;


/**
 Stops the preview and stops the ScanTrust frame processing.
 */
- (void)pauseScanning;


/**
 Restarts the ScanTrust frame processing. Should be used when the frame processing has been paused.
 Note: this will not have an impact on the camera or its preview, but will restart the response flow.
 */
- (void)restartProcessing;


/**
 Pauses the ScanTrust frame processing. This will pause the frame processing but camera and it's
 preview view will not be stopped.
 */
- (void)pauseProcessing;


/**
 Turns the torch on or off depending on the value of turnOn .

 @param turnOn A boolean value to turn on or off camera torch
 */
- (void)setCameraTorchModeOnOff:(BOOL)turnOn;


/**
 Setup the camera with given preview and prepares ScanTrust frame processing

 @param previewView The object of STPreviewView
 @param error If the camera fails to setup/prepare,
 upon return contains an instance of NSError that describes the problem.
 */
- (void)setUpCameraSessionWithPreview:(STPreviewView *)previewView
                                error:(NSError *)error;

/**
 Request permission for accessing device camera
 :nodoc:
 */
- (void)requestPersmissionForCamera;


/**
 To check whether camera access is granted by user or not

 @return If the user grants permission to use the hardware YES is returned; otherwise NO.
 The block will return immediately.
 :nodoc:
 */
- (BOOL)isAuthorizedForCamera;

@end
