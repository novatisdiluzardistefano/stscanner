/********* STScanner.m Cordova Plugin Implementation *******/
#import "STScanner.h"
#import "STScannerViewController.h"
@implementation STScanner

- (void)scan:(CDVInvokedUrlCommand*)command
{
    self.language = [[command.arguments objectAtIndex:0] objectAtIndex:0];
    self.callback = command.callbackId;
    if([command.methodName isEqualToString:@"check"]) {
        // Come faccio il check con questa libreria?
        [self scanBarcode];
    } else if ([command.methodName isEqualToString:@"scan"]) {
        [self scanBarcode];
    }
}

- (void)returnSuccess:(NSString*)data message:(NSString*)message info:(NSString*)info status:(BOOL)status{
    NSMutableDictionary* resultDict = [NSMutableDictionary new];
    resultDict[@"data"] = data;
    resultDict[@"message"] = message;
    resultDict[@"info"] = info;
    resultDict[@"status"] = status ? @YES : @NO;

    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:resultDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
        [self returnError:error.localizedDescription];
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        CDVPluginResult* result = [CDVPluginResult
                                   resultWithStatus: CDVCommandStatus_OK
                                   messageAsString: jsonString
                                   //messageAsDictionary:resultDict
                                   ];
        [self.commandDelegate sendPluginResult:result callbackId:self.callback];
    }
}

//--------------------------------------------------------------------------
- (void)returnError:(NSString*)data message:(NSString*)message {
    CDVPluginResult* result = [CDVPluginResult
                               resultWithStatus: CDVCommandStatus_ERROR
                               messageAsString: message
                               ];
    
    [self.commandDelegate sendPluginResult:result callbackId:self.callback];
}
//--------------------------------------------------------------------------
- (void)openDialog {
    [self.viewController
     presentViewController:self.scanViewController
     animated:true completion:nil
     ];
}

- (void)scanBarcode {
    self.scanViewController = [[STScannerViewController alloc] initWithPlugin:self callback:nil nibName:@"STScannerOverlay"];
     
    // delayed [self openDialog];
    [self performSelector:@selector(openDialog) withObject:nil afterDelay:1];
}
@end
