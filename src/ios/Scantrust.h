
/*************************************************************************
 *
 * SCANTRUST CONFIDENTIAL
 * __
 *
 *  [2015] - [2018] ScanTrust SA, PSE - D, EPFL Innovation Park, 1015 Lausanne, Switzerland
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of ScanTrust SA and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to ScanTrust SA
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from ScanTrust SA .
 */

//! Project version number for STAuthenticate.
FOUNDATION_EXPORT double STAuthenticateVersionNumber;

//! Project version string for STAuthenticate.
FOUNDATION_EXPORT const unsigned char STAuthenticateVersionString[];

#ifndef Scantrust_h
#define Scantrust_h

#import "STCameraManager.h"
#import "STLimitedCameraManager.h"

#import "STAuthResponse.h"
#import "STPreviewView.h"

#endif /* Scantrust_h */
