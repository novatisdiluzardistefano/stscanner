
/*************************************************************************
 *
 * SCANTRUST CONFIDENTIAL
 * __
 *
 *  [2015] - [2018] ScanTrust SA, PSE - D, EPFL Innovation Park, 1015 Lausanne, Switzerland
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of ScanTrust SA and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to ScanTrust SA
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from ScanTrust SA .
 */

/*!
 @file STCameraManager.h
 
 @brief This is the header file where all methods and enums contained for ST Authenticate.
 
 This file contains the method and properties decalaration. It's a singleton class which 
 helps to start/stop a camera session, Request camera permission and trigger web service 
 request to validate or authenticate scanned secure qr code
*/


#import <Foundation/Foundation.h>
#import "STPreviewView.h"
#import "STAuthRequest.h"
#import "STAuthResponse.h"
#import "STProduct.h"
#import "STBrand.h"

#pragma mark - Notification Names

/// Posted when scan is completed from ScanTrust Processing
static NSString * const STScanCompleteNotification = @"com.scantrust.enterprise.ios.authenticate.scancompelete";

/// Posted when scan fails from ScanTrust Processing
static NSString * const STScanFailNotification     = @"com.scantrust.enterprise.ios.authenticate.scanfail";

/// Posted during the scan from ScanTrust Processing
static NSString * const STScanProgressNotification = @"com.scantrust.enterprise.ios.authenticate.scanprogress";

/// Posted when camera zoom level changes from 1x to 4x
static NSString * const STCameraZoommingInNotificaiton  = @"com.scantrust.enterprise.ios.authenticate.zoommingin";

/// Posted when camera zooom level changes from 4x to 1x
static NSString * const STCameraZoommingOutNotification = @"com.scantrust.enterprise.ios.authenticate.zoommingout";

#pragma mark - Error Constants

/// Error Domain for Camera setup failures
static NSString * const STCameraErrorDomain        = @"com.scantrust.enterprise.ios.authenticate.STCameraErrorDomain";

#pragma mark - Constants

///
static NSString * const kScanProgress              = @"scan_progress";

///
static NSString * const kScanedQRCode              = @"qr_code";

///
static NSString * const kBlurScore                 = @"blur_score";

///
static NSString * const kZoomLevel                 = @"zoom_level";


/**
 The enum represent scan progress on real time from ScanTrust frame processing.
 */
typedef NS_ENUM(NSInteger, STScanState) {
    /// Reading the QR code
    STScanStateUnreadable,
    
    /// QR code read and processing
    STScanStateOk,
    
    /// QR code is small and not big enough
    STScanStateTooSmall,
    
    /// QR code is more then big enough
    STScanStateTooBig,
    
    /// QR code is blurry
    STScanStateBlurry,
    
    /// QR code is not a ScanTrust Code
    STScanStateNotProperietary,
    
    /// Secure fingerprint is out side of the frame
    STScanStateFPNotInFrame,
    
    /// No Auth code being detected
    STScanStateNoAuth,
    
    /// Scan is taking too long enable power saving mode
    STScanStatePowerSaving,
    
    /// QR code is not in center of the frame
    STScanStateQRCodeNotInCenter,
    
    /// Glare has been detected and no QR code found
    STScanStateGlareDetected,

    /// ScanTimedOut
    STScanStateScanTimeOut
};


/**
 A enum about the scan zoom level.
 */
typedef NS_ENUM(NSInteger, STScanZoomLevel) {
    /// Camera zoom is 1x. a.k.a no zoom
    STScanZoomLevel1x = 1,
    
    /// Camera zoom is 4x
    STScanZoomLevel4x = 4
};

@class STCameraManager;

/**
 Protocol methods to receive scan result from a ScanTrust frame processing
 */
@protocol STCameraManagerDelegate <NSObject>
@optional
/**
 Informs the delegate that the camera frame is processed by ScanTrust frame processing with a scan result
 
 @param cameraManager The limited camera manager object that captured the frame for processing
 @param scanState The STScanState instance representing a scan result
 */
- (void)cameraManager:(STCameraManager *)cameraManager didProcessFrameWithScanState:(STScanState)scanState;

/**
 Informs the delegate that the camera frame is completed processing by ScanTrust frame processing with a scan result
 
 @param cameraManager The limited camera manager object that captured the frame for processing
 @param authRequest The STAuthRequest instance representing a scanned code
 */
- (void)cameraManager:(STCameraManager *)cameraManager didCompleteScanWithAuthRequest:(STAuthRequest *)authRequest;
@end



/**
 The STCameraManager is a single point of the ScanTrust Processing and it does all the heavy lifting,
 setting up the camera and the preview, running the image processing on the incoming camera frames and
 reporting various outcomes
 */
@interface STCameraManager : NSObject

#pragma mark - Delegate

/**
 The object that acts as delegate for ScanTrust camera manager
 */
@property (nonatomic, strong) id<STCameraManagerDelegate> delegate;

#pragma mark - Initialization

/**
 Returns the shared camera manager instance
 
 @return The shared camera manager instance
 */
+ (STCameraManager *)sharedInstance;

#pragma mark - Camera
/**
 To get the camera preview object
 
 @return The STPreviewView object associated to camera manager instance
 :nodoc:
 */
- (STPreviewView *)getCameraPreviewView;

/**
 Starts the underlying camera object, its preview and starts the ScanTrust frame processing.
 */
- (void)startScanning;

/**
 Stops the preview and stops the ScanTrust frame processing.
 */
- (void)pauseScanning;

/**
 Restarts the ScanTrust frame processing. Should be used when the frame processing has been paused.
 Note: this will not have an impact on the camera or its preview, but will restart the response flow.
 */
- (void)restartProcessing;


/**
 Pauses the ScanTrust frame processing. This will pause the frame processing but camera and it's
 preview view will not be stopped.
 */
- (void)pauseProcessing;

/**
 Turns the torch on or off depending on the value of turnOn .
 
 @param turnOn A boolean value to turn on or off camera torch
 */
- (void)setCameraTorchModeOnOff:(BOOL)turnOn;



/**
 Request permission for accessing device camera
 :nodoc:
 */
- (void)requestPersmissionForCamera;

/**
 To check whether camera access is granted by user or not
 
 @return If the user grants permission to use the hardware YES is returned; otherwise NO.
 The block will return immediately.
 :nodoc:
 */
- (BOOL)isAuthorizedForCamera;

/**
 Setup the camera with given preview and prepares ScanTrust frame processing
 
 @param previewView The object of STPreviewView
 @param error If the camera fails to setup/prepare,
 upon return contains an instance of NSError that describes the problem.
 */
- (void)setUpCameraSessionWithPreview:(STPreviewView *)previewView
                                error:(NSError *)error;

/**
 To disable auto zoom feature of camera. When disabled default camera zoom level set to 4x
 :nodoc:
 */
- (void)disableAutoZoom;

/**
 To enable auto zoom feature of camera. When enabeld default camera zoom level set to 1x
 :nodoc:
 */
- (void)enableAutoZoom;

/**
 To take picture from the camera.
 
 @param completionHandler A block object to be exectued when taking a picture from camera is success.
 This block has the UIImage instace of captured image.
 :nodoc:
 */
- (void)takePicture:(void(^)(UIImage *caturedImage))completionHandler;

#pragma mark - Network request

/**
 To Authenticate a code scan from ScanTrust Processing

 @param request The STAuthRequest instance representing the scanned code
 @param completionBlock A block object to be executed when the network request finished sucessfully.
 This block has return values of STAuthResponse instance and instance of NSError, if network request fails
 */
- (void)authenticateScan:(STAuthRequest *)request
              completion:(void (^)(STAuthResponse *response, NSError *error))completionBlock;

#pragma mark - Helpers

/**
 To get the qr code minimum percent to scan non blurry image

 @return int value of code minimum percent
 :nodoc:
 */
- (int)getCodeMinPercent;

/**
 To identify the current device is supported phone for authentication of secure qr codes
 returns YES if the phone is supported and NO for non supported devices including iPad, iPod

 @return A boolean value representing current phone is supported by ScanTrust Frame processing or not
 :nodoc:
 */
- (BOOL)isPhoneSupported;


@end
