
/*************************************************************************
 *
 * SCANTRUST CONFIDENTIAL
 * __
 *
 *  [2015] - [2018] ScanTrust SA, PSE - D, EPFL Innovation Park, 1015 Lausanne, Switzerland
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of ScanTrust SA and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to ScanTrust SA
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from ScanTrust SA .
 */

#import <Foundation/Foundation.h>

/**
 A model class which represents the brand details and to represent the details about
 brand which was associcated to the scanned QR code
 */
@interface STBrand : NSObject <NSCoding, NSCopying>


/**
 The brand description as per ScanTrust Portal
 */
@property (nonatomic, strong) NSString *brandDescription;


/**
 The image URL string of brand
 */
@property (nonatomic, strong) NSString *image;


/**
 The URL string of brand
 */
@property (nonatomic, strong) NSString *url;


/**
 The name of the brand as per ScanTrust Portal
 */
@property (nonatomic, strong) NSString *name;


/**
 Static method to initialize the class with a dictionary

 @param dict NSDictionary object
 @return A instance of STBrand
 :nodoc:
 */
+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;


/**
 Init the class with a dictionary

 @param dict NSDictionary object
 @return A instance of STBrand
 :nodoc:
 */
- (instancetype)initWithDictionary:(NSDictionary *)dict;


/**
 Dictonary representation of STBrand model object

 @return NSDictionary object contains STBrand model object parameter details
 :nodoc:
 */
- (NSDictionary *)dictionaryRepresentation;

@end
