//
//  STScannerViewController.m
//  Vericode
//
//  Created by stefano luzardi on 22/08/18.
//

#import <Foundation/Foundation.h>
#import "STScannerViewController.h"
#import "STScanner.h"

@interface STScannerViewController ()
    @property (nonatomic) BOOL isScanInProgress;
    @property (nonatomic, strong) UIActivityIndicatorView *indicator;
@end

@implementation STScannerViewController

SystemSoundID _soundFileObject;
NSDictionary* _messages;
NSString* _lastQrCode;
BOOL _returnOnFailure;
NSString* _lastFailureReason;

- (id)initWithPlugin:(STScanner*)plugin
            callback:(NSString*)callback
             nibName:(NSString*)nibName{
    self = [super initWithNibName:nibName bundle:nil];
    if (!self) return self;
    self.plugin = plugin;
    
    CFURLRef soundFileURLRef  = CFBundleCopyResourceURL(CFBundleGetMainBundle(),
                                                        CFSTR("STScanner/beep"),
                                                        CFSTR ("caf"), NULL);
    AudioServicesCreateSystemSoundID(soundFileURLRef, &_soundFileObject);

    // Load resources messages
    NSString *fname = [[NSBundle mainBundle] pathForResource:@"STScanner" ofType:@"strings"];
    _messages = [NSDictionary dictionaryWithContentsOfFile:fname];
    _lastQrCode = @"";
    _returnOnFailure = NO;
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupCamera];
    [self setupLoadingIndicator];
    [self startScanning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

#pragma mark - Camera Methods

- (STCameraManager *)cameraManager {
    return [STCameraManager sharedInstance];
}

- (void)setupCamera {
    if (![[self cameraManager] isAuthorizedForCamera]) {
        [[self cameraManager] requestPersmissionForCamera];
    }
    
    NSError *error;
    [[self cameraManager] setUpCameraSessionWithPreview:self.preview error:error];
    if (error) {
        // Handle camera setup failure if any
    }
    [self cameraManager].delegate = self;
}

- (void)startScanning {
    //self.isScanInProgress = YES;
    [[STCameraManager sharedInstance] setCameraTorchModeOnOff:YES];
    [[self cameraManager] startScanning];
    /*
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(scanFailed:)
                                                 name:STScanFailNotification
                                               object:nil];
     */
}

- (void)stopScanning {
    [[STCameraManager sharedInstance] setCameraTorchModeOnOff:NO];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
               name:STScanFailNotification
             object:nil];

    [[self cameraManager] pauseScanning];
    self.isScanInProgress = NO;
}

- (void)scanFailed:(NSNotification *)notification {
    NSDictionary *scanInfo = notification.userInfo;
    _lastQrCode = [scanInfo objectForKey:kScanedQRCode] != nil ? [scanInfo objectForKey:kScanedQRCode]: _lastQrCode;
    
    if(_returnOnFailure) {
        _returnOnFailure = NO;
        [self barcodeScanSucceeded:_lastQrCode message:@"fail" info:_lastFailureReason status:NO];
    }
}


#pragma mark - STCameraManagerDelegate methods
- (NSString*) getMessage:(NSString*)msgCode {
    return [_messages objectForKey:[NSString stringWithFormat:@"%@_%@", _plugin.language, msgCode]];
}

- (void)cameraManager:(STCameraManager *)cameraManager didProcessFrameWithScanState:(STScanState)scanState {
    if(!self.isScanInProgress) {
        // Va messo qua altrimenti questa funzione non viene più chiamata
        self.isScanInProgress = YES;
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(scanFailed:)
                                                     name:STScanFailNotification
                                                   object:nil];
    }
    NSString *scanStateText = @"";
    switch (scanState) {
        case STScanStateOk:
            //scanStateText = @"Please don't move";
            scanStateText = [self getMessage:@"STScanStateOk"];
            break;
        case STScanStateBlurry:
            //scanStateText = @"Blurry";
            scanStateText = [self getMessage:@"STScanStateBlurry"];
            break;
        case STScanStateNotProperietary:
            //scanStateText = @"Not a ScanTrust Code";
            scanStateText = [self getMessage:@"STScanStateNotProperietary"];
            // la notifica viene chiamata dopo
            _returnOnFailure = YES;
            _lastFailureReason = @"non_proprietario";
            //[self barcodeScanSucceeded:_lastQrCode message:@"fail" info:@"non_proprietario" status:NO];
            break;
        case STScanStateFPNotInFrame:
            //scanStateText = @"FingerPrint is not in frame";
            scanStateText = [self getMessage:@"STScanStateFPNotInFrame"];
            break;
        case STScanStateTooBig:
            //scanStateText = @"Too Big - Move back";
            scanStateText = [self getMessage:@"STScanStateTooBig"];
            break;
        case STScanStateTooSmall:
            //scanStateText = @"Too Small - Move Closer";
            scanStateText = [self getMessage:@"STScanStateTooSmall"];
            break;
        case STScanStateUnreadable:
            //scanStateText = @"Can't read any QR Code";
            scanStateText = [self getMessage:@"STScanStateUnreadable"];
            break;
        case STScanStateQRCodeNotInCenter:
            //scanStateText = @"QR code is not in center";
            scanStateText = [self getMessage:@"STScanStateQRCodeNotInCenter"];
            break;
        case STScanStateGlareDetected:
            //scanStateText = @"Possible Glare detected";
            scanStateText = [self getMessage:@"STScanStateGlareDetected"];
            break;
        case STScanStateScanTimeOut:
            //scanStateText = @"Couldn't obtain a scan of sufficient quality.";
            scanStateText = [self getMessage:@"STScanStateScanTimeOut"];
            // la notifica viene chiamata dopo
            _returnOnFailure = YES;
            _lastFailureReason = @"qualita_bassa";
            //[self barcodeScanSucceeded:_lastQrCode message:@"fail" info:@"qualita_bassa" status:NO];
            break;
        default:
            scanStateText = @"";
            break;
    }
    
    // UI updates must be on main thread
    dispatch_async(dispatch_get_main_queue(), ^{
        self.scanStateLabel.text = scanStateText;
    });
}

- (void)cameraManager:(STCameraManager *)cameraManager didCompleteScanWithAuthRequest:(STAuthRequest *)authRequest {
    // App must send the version, else request will fail
    authRequest.mobileVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    
    [self stopScanning];
    [self startLoading];
    [[self cameraManager] authenticateScan:authRequest completion:^(STAuthResponse *response, NSError *error) {
        [self stopLoading];
        if (error) {
            [self barcodeScanFailed:error.localizedDescription];
            return;
        }
        
        if ([response.reason isEqualToString:@"auth"]) {
            if ([response.result isEqualToString:@"ok"]) {
                // Genuine
                [self barcodeScanSucceeded:authRequest.message
                                   message:response.result
                                      info:@"ok" status:YES];
            }
            else if ([response.result isEqualToString: @"fail"]) {
                // Counterfeit/ Unreadable
                if ([response.authFailureMode isEqualToString:@"auth-failed"]) {
                    [self barcodeScanSucceeded:authRequest.message
                                       message:response.result
                                          info:@"auth_failed" status:NO];
                }
                else if ([response.authFailureMode isEqualToString:@"unreadable"]) {
                    [self barcodeScanSucceeded:authRequest.message
                                       message:response.result
                                          info:@"unreadable" status:NO];
                }
            }
        }
        else {
            [self barcodeScanSucceeded:authRequest.message
                               message:response.result
                                  info:@"ok" status:YES];
        }
    }];
}

#pragma mark - Helper Methods

- (void)setupLoadingIndicator {
    self.indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.indicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
    self.indicator.center = self.view.center;
    [self.view addSubview:self.indicator];
    [self.indicator bringSubviewToFront:self.view];
}

- (void)startLoading {
    if (self.indicator) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        [self.indicator startAnimating];
    }
}

- (void)stopLoading {
    if (self.indicator) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [self.indicator stopAnimating];
    }
}

- (void)barcodeScanDone:(void (^)(void))callbackBlock {
    [[STCameraManager sharedInstance] setCameraTorchModeOnOff:NO];
    [[self cameraManager] pauseScanning];
    [self stopLoading];
    [self dismissViewControllerAnimated:YES completion:callbackBlock];
}

#pragma mark - Action methods
- (IBAction)cancelButtonTapped:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self barcodeScanDone:^{
            [self.plugin returnSuccess:@"" message:@"cancelled" info:@"" status:NO];
        }];
    });
}

//--------------------------------------------------------------------------
- (void)barcodeScanSucceeded:(NSString*)data
                     message:(NSString*)message
                        info:(NSString*)info
                      status:(BOOL)status{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self barcodeScanDone:^{
            [self.plugin returnSuccess:data message:message info:info status:status];
        }];
        AudioServicesPlaySystemSound(_soundFileObject);
    });
}

//--------------------------------------------------------------------------
- (void)barcodeScanFailed:(NSString*)message {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self barcodeScanDone:^{
            [self.plugin returnError:message];
        }];
    });
}
@end
