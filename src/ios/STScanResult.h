
/*************************************************************************
 *
 * SCANTRUST CONFIDENTIAL
 * __
 *
 *  [2015] - [2018] ScanTrust SA, PSE - D, EPFL Innovation Park, 1015 Lausanne, Switzerland
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of ScanTrust SA and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to ScanTrust SA
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from ScanTrust SA .
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "STCameraManager.h"


/**
 The scan result from ScanTrust limited camera manager represents the processed frame result
 */
@interface STScanResult : NSObject


/**
 The error corrected scaned code data decoded into a human-readable string.
 */
@property (nonatomic, strong) NSString *data;


/**
 The scan state of the processed frame and representing a code belonging to ScanTrust has been detected or not
 */
@property (nonatomic) STScanState scanState;
@end
