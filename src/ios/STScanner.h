#import <Cordova/CDV.h>
#import <UIKit/UIKit.h>

@interface STScanner : CDVPlugin {}
@property (retain) UIViewController* scanViewController;
@property (retain) NSString* callback;
@property (weak, nonatomic) NSString* language;
- (void)scan:(CDVInvokedUrlCommand*)command;
- (void)openDialog;
- (void)returnSuccess:(NSString*)data message:(NSString*)message info:(NSString*)info status:(BOOL)status;
- (void)returnError:(NSString*)message;
@end
