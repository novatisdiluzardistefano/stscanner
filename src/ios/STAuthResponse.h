
/*************************************************************************
 *
 * SCANTRUST CONFIDENTIAL
 * __
 *
 *  [2015] - [2018] ScanTrust SA, PSE - D, EPFL Innovation Park, 1015 Lausanne, Switzerland
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of ScanTrust SA and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to ScanTrust SA
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from ScanTrust SA .
 */

#import <Foundation/Foundation.h>

@class STProduct, STBrand;


/**
 The enum about code activation status
 */
typedef NS_ENUM(NSInteger, STActivationStatus) {
    ///
    STActivationStatusInActive      = 0,

    ///
    STActivationStatusActive        = 1,
    
    ///
    STActivationStatusBlackListed   = 2,
};


/**
 A model class which helps to get all the scan details from back end server response
 and which can have all the details about the scan
 */
@interface STAuthResponse : NSObject <NSCoding, NSCopying>


///
@property (nonatomic, assign) double serverScore;

///
@property (nonatomic, strong) NSString *uuid;

///
@property (nonatomic, strong) NSString *consumerUrl;

///
@property (nonatomic, strong) STProduct *product;

///
@property (nonatomic, strong) STBrand *brand;

///
@property (nonatomic, assign) BOOL isBlacklisted;

///
@property (nonatomic, assign) double activationStatus;

///
@property (nonatomic, strong) NSString *app;

///
@property (nonatomic, strong) NSString *reason;

///
@property (nonatomic, strong) NSString *authFailureMode;

///
@property (nonatomic, assign) double unreadableRetries;

///
@property (nonatomic, strong) NSString *result;

///
@property (nonatomic, assign) double trainingStatus;

///
@property (nonatomic, assign) double blacklistReason;

///
@property (nonatomic, strong) NSDictionary *scmData;

///
@property (nonatomic, strong) NSString *gaKeyApp;

///
@property (nonatomic, assign) BOOL isConsumed;

///
@property (nonatomic, assign) BOOL byPassScanResult;

///
@property (nonatomic, assign) BOOL requireGeoLocaion;

/**
 Static method to initialize the class with a dictionary
 
 @param dict NSDictionary object
 @return A instance of STBrand
 :nodoc:
 */
+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;


/**
 Init the class with a dictionary
 
 @param dict NSDictionary object
 @return A instance of STBrand
 :nodoc:
 */
- (instancetype)initWithDictionary:(NSDictionary *)dict;


/**
 Dictonary representation of STBrand model object
 
 @return NSDictionary object contains STBrand model object parameter details
 :nodoc:
 */
- (NSDictionary *)dictionaryRepresentation;


/**
 To get the error code

 @return A NSString object representing the error code
 :nodoc:
 */
- (NSString *)getErrorCode;
@end
