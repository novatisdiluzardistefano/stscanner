
/*************************************************************************
 *
 * SCANTRUST CONFIDENTIAL
 * __
 *
 *  [2015] - [2018] ScanTrust SA, PSE - D, EPFL Innovation Park, 1015 Lausanne, Switzerland
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of ScanTrust SA and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to ScanTrust SA
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from ScanTrust SA .
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

/**
 A model class which helps to get all the scanned QR details and which can have all the details
 about the scanned QR code. After the scan is success, this class will help to trigger the request to the backend server
 */
@interface STAuthRequest : NSObject <NSCoding, NSCopying>

///
@property (nonatomic, strong) NSString *deviceModel;

///
@property (nonatomic, strong) NSString *deviceOs;

///
@property (nonatomic, strong) NSString *message;

///
@property (nonatomic, strong) NSString *deviceImei;

///
@property (nonatomic, assign) double locationPositioning;

///
@property (nonatomic, assign) double mobileScore;

///
@property (nonatomic, assign) BOOL unreadable;

///
@property (nonatomic, assign) BOOL synchronized;

///
@property (nonatomic, assign) double locationLatitude;

///
@property (nonatomic, strong) NSString *app;

///
@property (nonatomic, assign) double mobileTimestamp;

///
@property (nonatomic, assign) double locationLongitude;

///
@property (nonatomic, strong) NSString *deviceKernel;

///
@property (nonatomic, strong) NSString *deviceOsVersion;

///
@property (nonatomic, strong) NSString *mobileVersion;

///
@property (nonatomic, strong) NSArray *qrCorners;

///
@property (nonatomic, strong) NSArray *cropOffset;

///
@property (nonatomic) BOOL isDeviceSupported;

///
@property (nonatomic, strong) NSString *preferredEndPoint;

///
@property (nonatomic) BOOL isNonAuthCodeScanned;

///
@property (nonatomic, strong) NSString *customKey;

///
@property (nonatomic,strong) UIImage *fingerprintImage;

///
@property (nonatomic,strong) UIImage *qrImage;

///
@property (nonatomic,strong) NSString *accessToken; // For logged in users 

/**
 Static method to initialize the class with a dictionary
 
 @param dict NSDictionary object
 @return A instance of STBrand
 :nodoc:
 */
+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;


/**
 Init the class with a dictionary
 
 @param dict NSDictionary object
 @return A instance of STBrand
 :nodoc:
 */
- (instancetype)initWithDictionary:(NSDictionary *)dict;


/**
 Dictonary representation of STBrand model object
 
 @return NSDictionary object contains STBrand model object parameter details
 :nodoc:
 */
- (NSDictionary *)dictionaryRepresentation;


/**
 Update the model with a CLLocation object

 @param location A CLLocation object contains latitude and longitude
 :nodoc:
 */
- (void)updateWithLocation:(CLLocation *)location;

@end
