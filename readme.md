Per l'installazione
- ionic cordova plugin add /<directory>/STScanner
(per rimuovere 'ionic cordova plugin remove cordova-plugin-stScanner)

Per il funzionamento di iOS
dopo avere aggiunto il plugin al progetto:
- Sistemare il signin
- Aggiungere alle risorse del progetto il file di bundle STScanner.bundle che si troverà nell'indirizzarion Resources del plugin
- Verificare la presenza dei linker flag: -ObjC -lstdc++ -liconv


aggiungere il seguente provider StScannerProvider:
import { Injectable } from '@angular/core';
import { Plugin, Cordova} from '@ionic-native/core';
/*
  Generated class for the StScannerProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/

@Plugin ({
  pluginName: "stscanner",
  plugin: "cordova-plugin-stScanner",
  pluginRef: "STScanner",
  repo: "https://bitbucket.org/novatisdiluzardistefano/stscanner.git",
  platforms: ["iOS"]
})

@Injectable()
export class StScannerProvider {
  @Cordova()
  scan(success:any, error: any, pars: any[]): Promise<any>
  {
    return;
  }
}

